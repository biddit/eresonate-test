const express = require('express');
const path = require('path');
const app = express();
app.use(express.static(path.join(__dirname, 'build')));

app.post('/send_report', function(req, res) {

    setTimeout(function() {
        res.json({status: 'ok'});
    }, 2000)
});

app.listen(process.env.PORT || 8080);