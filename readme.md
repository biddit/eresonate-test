# eResonate Test

This application implements the requirements outlined in [the provided requirements document](./Requirements.pdf)
and the designs provided in [this Zeplin project](https://zpl.io/2peJNYN).

## Background

This demo runs two different web servers:
- A [Create React App](https://github.com/facebook/create-react-app) based server (default port 3000), which builds
and serves the front-end code for the application.
- A simple [Express.js](https://expressjs.com/) server (default port 8080) to mock the API call as required per the 
specification.

## Setup instructions

After cloning the repository, install the node modules:

```bash
npm install
```

Next run the nodejs server:

```bash
node server.js
```

In a separate terminal window, run the `create react app` build process to serve the front end:

```bash
npm start
```

The last step should open your web browser by default, but if not, navigate to 
[http://localhost:3000](http://localhost:3000)