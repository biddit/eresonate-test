import React, { Component } from "react";
import SimpleReactValidator from 'simple-react-validator';
import "../css/ReportForm.css";

import Input from "./Input";
import TextArea from "./TextArea";
import Button from "./Button";

class ReportForm extends Component {
    constructor(props) {

        super(props);

        this.state = {
            report: {
                name: "",
                phone: "",
                email: "",
                message: ""
            },
            submitting: false
        };

        this.handleInput = this.handleInput.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);

        this.validator = new SimpleReactValidator();
    }

    handleInput(e) {
        let value = e.target.value;
        let name = e.target.name;
        this.setState(
            prevState => ({
                report: {
                    ...prevState.report,
                    [name]: value
                }
            })
        );
    }

    handleFormSubmit(e) {
        e.preventDefault();

        if(this.state.submitting) {
            // do nothing
        } else if(!this.validator.allValid()) {
            this.validator.showMessages();
            this.forceUpdate();
        } else {
            let report = this.state.report;
            this.setState({submitting: true});
            console.log('submitting',this.state.report);
            fetch("/send_report", {
                method: "POST",
                body: JSON.stringify(report),
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json"
                }
            }).then(response => {
                this.setState({submitting: false});
                response.json().then(data => {
                    console.log("Send Data Successful", data);
                });
            });
        }
    }

    render() {
        const isSubmitting = this.state.submitting;

        return (
            <form className="form-container" onSubmit={this.handleFormSubmit}>
                <h1>Report a Problem</h1>
                <Input
                    type={"text"}
                    title={"Your Name"}
                    name={"name"}
                    value={this.state.report.name}
                    handlechange={this.handleInput}
                />
                {this.validator.message('name', this.state.report.name, 'required')}
                <Input
                    type={"number"}
                    title={"Phone Number"}
                    name={"phone"}
                    value={this.state.report.phone}
                    handlechange={this.handleInput}
                />
                {this.validator.message('phone', this.state.report.phone, 'required')}
                <Input
                    type={"email"}
                    title={"Email"}
                    name={"email"}
                    value={this.state.report.email}
                    handlechange={this.handleInput}
                />
                {this.validator.message('email', this.state.report.email, 'required|email')}
                <TextArea
                    title={"Message"}
                    rows={10}
                    value={this.state.report.message}
                    name={"message"}
                    handlechange={this.handleInput}
                />
                {this.validator.message('message', this.state.report.message, 'required')}
                <Button
                    action={this.handleFormSubmit}
                    type={"primary"}
                    title={"Submit"}

                />
                {isSubmitting ? <p>Submitting report...</p> : ""}
            </form>

        );
    }
}

export default ReportForm;
