import React from "react";

const Input = props => {
    console.log(props);
    return (
        <div className="form-group">
            <label htmlFor={props.name}>
                {props.title}
            </label>
            <input
                id={props.name}
                name={props.name}
                type={props.inputType}
                value={props.value}
                onChange={props.handlechange}
                placeholder={props.placeholder}
            />
        </div>
    );
};

export default Input;
