import React, { Component } from "react";
import { render } from "react-dom";
import ReportForm from "./components/ReportForm";
import "./css/index.css";

class App extends Component {
    render() {
        return (
            <div className="col-md-6">
                <ReportForm />
            </div>
        );
    }
}

render(<App />, document.getElementById("root"));
